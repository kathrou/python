import random
import hangman_art
import hangman_words

chosen_word = random.choice(hangman_words.word_list)
word_length = len(chosen_word)

end_of_game = False
lives = 6

print(hangman_art.logo)

lives = 6
print(hangman_art.stages[lives])
display = []

for letter in chosen_word:
  display.append('_')


while '_' in display and lives > 0:
  guess = input("Guess a letter: ").lower()
  if guess in display:
    print('You have already guessed this letter')
  for index, letter in enumerate(chosen_word):
    if letter == guess:
      display[index] = guess
  if guess not in chosen_word:
    print(f'Letter {guess} is not part of the word, you lose a life!')
    lives -= 1
  print(f"{' '.join(display)}")
  print(hangman_art.stages[lives])
  

if lives > 0: 
  print('\nYou win!')
else:
  print(f'\nYou lose. The word was {chosen_word}')
    #TODO-2: - Import the stages from hangman_art.py and make this error go away.
