
print("Welcome to Treasure Island.")
print("Your mission is to find the treasure.") 

#https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Treasure%20Island%20Conditional.drawio#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1oDe4ehjWZipYRsVfeAx2HyB7LCQ8_Fvi%26export%3Ddownload

#Write your code below this line 👇

direction = input("You are at a crossroad, where would you like to go? Type 'left' or 'right'\n").lower()

if direction == 'left':
  lake = input("You are at a lake with an island in the middle, type 'wait' to wait for a boat or 'swim' to swim across\n").lower()
  if lake == 'wait':
    door = input("You arrive unharmed. There is a house with 3 doors, which do you choose? Type 'red', 'yellow' or 'blue'?\n").lower()
    if door == 'red':
      print("Burned by fire. Game over")
    elif door == 'blue':
      print('Eaten by beasts. Game Over')
    elif door == 'yellow':
      print('You win!')
    else:
      print('Game Over')
  else: 
    print('Attacked by trout. Game over')
else:
  print("You have fallen into a hole. Game Over.")

