rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

#Write your code below this line 👇

import random

user_choice = int(input("What do you choose? Type 0 for rock, 1 for paper and 2 for scissors\n"))

images = [rock, paper, scissors]

options = ['rock', 'paper', 'scissors']

computer_response = random.randint(0,2)

if  user_choice < 0 or user_choice > 2:
  print("Invalid input. Please choose between 1, 2 or 3")
else:
  print(f"""

    You chose {options[user_choice]}:

    {images[user_choice]}
     
    Computer chose {options[computer_response]}:

    {images[computer_response]}
""")
  if options[user_choice] == 'rock':
    if computer_response == 0: 
      print("You draw")
    elif computer_response == 1:
      print("You lose")
    else:
      print("You win")
  elif options[user_choice] == 'paper':
    if computer_response == 0: 
      print("You win")
    elif computer_response == 1:
      print("You draw")
    else:
      print("You lose")
  else:
    if computer_response == 0: 
      print("You lose")
    elif computer_response == 1:
      print("You win")
    else:
      print("You draw")

  
  