#Password Generator Project
import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the PyPassword Generator!")
nr_letters= int(input("How many letters would you like in your password?\n")) 
nr_symbols = int(input(f"How many symbols would you like?\n"))
nr_numbers = int(input(f"How many numbers would you like?\n"))


total_length = nr_letters + nr_numbers + nr_symbols

password = ""

for n in range(0, nr_letters):
  password += random.choice(letters)

for n in range(0, nr_symbols):
  password += random.choice(symbols)

for n in range(0, nr_numbers):
  password += random.choice(numbers)

easy_password = password
hard_password = list(password)

for n in range(0, len(password)): 
  index1 = random.randint(0, len(password) - 1)
  index2 = random.randint(0, len(password) - 1)
  c = hard_password[index1] 
  hard_password[index1] = hard_password[index2]
  hard_password[index2] = c

hard_password = ''.join(hard_password)

print(f"Your easy password is {easy_password}")

print(f"Your hard password is {hard_password}")



